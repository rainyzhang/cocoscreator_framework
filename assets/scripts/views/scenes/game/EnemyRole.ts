import {CCComponent, CCNode, CCParticleSystem} from "../../../core/base/CCComponent";
import SpriteFrameAnimationComponent from "../../../core/components/SpriteFrameAnimationComponent";


const {ccclass, property} = cc._decorator;
/*
*ClassName:EnemyRole
*Description:TODO
*Date:2019/12/2 22:57
*/
@ccclass
export default class EnemyRole extends cc.Component {

    @property(cc.Node)
    enemyGunImg:cc.Node = null;

    @property({type:cc.Node})
    enemyBulletImg:cc.Node = null;

    @property(cc.Node)
    enemyHeroImg:cc.Node = null;

    @property(cc.ParticleSystem)
    enemyDieParticle:cc.ParticleSystem = null;

    _winSize = null;
    cloumn = null;

    callBack = null;

    _hitHeroCallBack = null;

    onLoad () {
        this._winSize = cc.winSize;
        // this.enemyHeroImg.active = false;
        // this.enemyGunImg.active = false;
    }

    //敌人运动
    enemyAni (){
        this.enemyHeroImg.getComponent(SpriteFrameAnimationComponent).playAni("enemy", 3, 0.1, true);
    }

    //调整敌方柱子高度
    setColumnHight (){
        //随机获取高度
        let y = Math.floor(Math.random() * -250) - 100;
        this.cloumn.position = cc.v2(this._winSize.width / 2 + 100, y);
    }

    //敌人进场动作
    comeOnAni (){
        this.node.active = true;
        // this.setColumnHight();
        // let w = Math.floor(Math.random() * (this._winSize.width / 4));

        this.enemyHeroImg.active = true;
        this.enemyGunImg.active = true;
        this.enemyGunImg.position = cc.v2(0,24.8);
        this.enemyHeroImg.position = cc.v2(0,0);
        this.enemyBulletImg.position = cc.v2(0,0);
        this.enemyAni();

        // this.node.runAction(cc.sequence(cc.moveTo(1.0, cc.v2(300, this.node.position.y)), cc.callFunc(() =>{
        //     // this.enemyHeroImg.active = true;
        //     // this.enemyGunImg.active = true;
        //     // this.enemyAni();
        // }, this)));
    }

    //敌方柱子运动
    enemyMove (){

        this.enemyHeroImg.active = false;
        this.enemyGunImg.active = false;

        // this.node.runAction(cc.sequence(cc.moveTo(1.0, cc.v2(-this._winSize.width / 2 - 100, this.node.position.y)), cc.callFunc(() =>{
        //     if(this.callBack){
        //         this.callBack();
        //     }
        // })));
        if(this.callBack){
            this.callBack();
        }
        // this.node.runAction(cc.sequence(cc.moveTo(1.0, cc.v2(300, 200)), cc.callFunc(() =>{
        //     if(this.callBack){
        //         this.callBack();
        //     }
        // })));

    }

    //获取敌方子弹的世界坐标
    enemyBulletWorldPos (){
        let pos = this.node.convertToWorldSpaceAR(cc.v2(this.enemyGunImg.position));
        return pos;
    }

    //设置炮的角度
    setGunAngle (angle){
        this.enemyGunImg.angle = angle;
    }

    //炮运动
    gunAni (len){
        let bulletPos = this.enemyBulletImg.position;
        this.enemyBulletImg.runAction(cc.sequence(cc.moveTo(0.3, cc.v2(len, 0)), cc.callFunc(() =>{
            if(this._hitHeroCallBack){
                this._hitHeroCallBack();
            }
            this.enemyBulletImg.position = bulletPos;
        })));
    }

    //敌方英雄死亡动画
    enemyDie (){
        this.enemyDieParticle.node.active = true;
        this.enemyDieParticle.stopSystem();
        this.enemyDieParticle.resetSystem();

        //隐藏敌方英雄
        this.enemyGunImg.active = false;
        this.enemyHeroImg.active = false;
    }

    //播放音效
    playSound (name:string, isLoop:boolean){
        cc.loader.loadRes(name, cc.AudioClip, function (err, clip) {
            if(err){
                return;
            }
            var audioID = cc.audioEngine.playEffect(clip, isLoop);
        });
    }

    //运动完成的回调
    finishCallBack (callBack){
        this.callBack = callBack;
    }

    //打中我方英雄后的回调
    hitHeroCallBack (callBack){
        this._hitHeroCallBack = callBack;
    }

    // called every frame
    update (dt) {

    }
}