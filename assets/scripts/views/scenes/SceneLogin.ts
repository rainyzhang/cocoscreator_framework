import UIView from "../../core/mvcFrame/view/UIView";
import { UIViewSceneLoginMsg } from "../../eventMsg/AllEventMsg";
import TMessage from "../../core/mvcFrame/TMessage";
import SceneGame from "./SceneGame";
import UIManager from "../../manager/UIManager";
import UIBaseView, { UIClass } from "../../core/mvcFrame/view/UIBaseView";
import { CCLabel } from "../../core/base/CCComponent";

const { ccclass, property } = cc._decorator;
/*
*ClassName:SceneLogin
*Description:TODO
*Date:2019/10/26 0:58
*/
@ccclass
export default class SceneLogin extends UIView {

    protected static prefabUrl = "prefabs/scenes/SceneLogin";
    protected static className = "SceneLogin";

    onLoad() {
        super.onLoad()
    }

    Show(msg: UIViewSceneLoginMsg = null) {
        super.Show(msg);

        console.log("登录场景");

        // TMessage.NotifyShowScene(SceneGame);
        let lab_start: CCLabel = this["@lab_start_Label"];
        lab_start.string = "asdsad";
        this["@lab_start_Label"].string = "asdsad";


        let prefabs = [];
        // prefabs.push("prefabs/panels/rank/PanelRankView");
        // prefabs.push("prefabs/scenes/SceneGame");
        prefabs.push(this.add(SceneGame));
        UIManager.instance.showLoadingScene(prefabs, () => {
            //console.error("加载成功")
        });

    }

    public add<T extends UIBaseView>(uiClass: UIClass<T>): string {
        return uiClass.getUrl();
    }

    //进入主场景
    public OnBtnClickGoToSceneGame() {
        TMessage.NotifyShowScene(SceneGame);
    }


}