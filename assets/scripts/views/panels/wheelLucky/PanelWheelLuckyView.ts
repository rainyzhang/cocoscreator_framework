import UIView from "../../../core/mvcFrame/view/UIView";
import { UIViewPanelWheelLuckyViewMsg } from "../../../eventMsg/AllEventMsg";
import { stringFormat } from "../../../utils/Utils";
import { CCSprite, CCLabel } from "../../../core/base/CCComponent";

const { ccclass, property } = cc._decorator;
/*
*ClassName:PanelWheelLuckyView
*Description:TODO
*Date:2019/11/5 22:23
*/
@ccclass
export default class PanelWheelLuckyView extends UIView {

    protected static prefabUrl = "prefabs/panels/wheelLucky/PanelWheelLuckyView";
    protected static className = "PanelWheelLuckyView";

    @property(cc.Sprite)
    roundBg: cc.Sprite = null;

    @property(cc.Label)
    lab_remain_count: cc.Label = null;

    private isActionEnd: boolean = false;

    private canDoRoundCount: number = 5;
    private roundCount: number = 0;
    private isMessageBack: boolean = false;
    private roundEndIndex: number = -1;


    onLoad() {
        super.onLoad();
    }
    onResetData() {

        this.roundBg.node.angle = 0;
        this.isActionEnd = true;
        this.roundCount = 0;
        this.isMessageBack = false;
        this.roundEndIndex = -1;
        this.lab_remain_count.string = stringFormat("剩余{0}次", this.canDoRoundCount);
    }
    public Show(msg: UIViewPanelWheelLuckyViewMsg = null) {
        super.Show();

        this.onResetData();
    }


    startBtnClick() {
        if (this.canDoRoundCount <= 0) {
            console.log("机会不够了");
            return;
        }
        if (!this.isActionEnd)
            return;
        this.canDoRoundCount--;
        this.lab_remain_count.string = stringFormat("剩余{0}次", this.canDoRoundCount);
        //转盘开始转动
        this.isActionEnd = false;
        this.roundBg.node.angle = 0;
        console.log("startBtnClick    this.roundBg.node.angle=" + this.roundBg.node.angle);

        this.rotateActFix({ index: 7, chance: 5 });
        //this.doActionFunc();
    }

    //协议未回来
    private doActionFunc() {
        let act1 = null;
        // if (this.roundCount === 0){
        //     act1 = cc.rotateBy(1.0,360);
        //     act1.easing(cc.easeIn(1.0));
        // }else{
        //     act1 = cc.rotateBy(0.5,360);
        // }
        act1 = cc.rotateBy(0.5, 360);
        let finished = cc.callFunc(() => {
            if (this.isMessageBack) {// || self.roundCount > 5
                this.roundBg.node.stopActionByTag(10000);
                this.roundCount = 0;
                //self.rotateActFix1();
            }
            else {
                this.doActionFunc();
            }
        });
        this.roundCount += 1;
        let action1 = cc.sequence(act1, finished);
        action1.setTag(10000);
        this.roundBg.node.runAction(action1);
    }

    //协议回来
    rotateActFix(data: any) {
        this.roundEndIndex = data.index;
        this.rotateActFix1();
    }
    rotateActFix1() {
        this.isMessageBack = true;
        let act1 = cc.rotateBy(0.5, +360);
        let finished1 = cc.callFunc(function () {
            if (this.roundCount > 5) {
                this.roundBg.node.stopActionByTag(10001);
                this.rotateActStop();
            } else {
                this.rotateActFix1();
            }
        }.bind(this));
        let action1 = cc.sequence(act1, finished1);
        action1.setTag(10001);
        this.roundBg.node.runAction(action1);
        this.roundCount += 1;
    }

    //动作渐停
    rotateActStop() {
        this.isMessageBack = false;
        let act1 = cc.rotateTo(2.0, 360 + 360 + 22.5 - this.roundEndIndex * 45);
        act1.easing(cc.easeOut(2.0));
        let finishEnd = cc.callFunc(() => {
            this.isActionEnd = true;
            this.roundBg.node.stopActionByTag(10002);
            this.roundCount = 0;
            console.error("最后的index=", this.roundEndIndex);
            console.log("rotateActStop    this.roundBg.node.rotation=" + this.roundBg.node.angle);
        });
        let actionSeq = cc.sequence(act1, finishEnd);
        actionSeq.setTag(10002);
        this.roundBg.node.runAction(actionSeq);
    }

    public Hide() {
        if (!this.isActionEnd) {
            return;
        }
        super.Hide();

        this.roundBg.node.stopActionByTag(10001);
        this.roundBg.node.stopActionByTag(10000);
        this.roundBg.node.stopActionByTag(10002);
    }

    protected onEnable(): void {
        super.onEnable();
    }

    protected onDisable(): void {
        super.onDisable();
    }
}