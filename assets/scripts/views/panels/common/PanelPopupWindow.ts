import UIView from "../../../core/mvcFrame/view/UIView";
import {UIViewPanelPopupWindowMsg} from "../../../eventMsg/AllEventMsg";

const {ccclass, property} = cc._decorator;
/*
*ClassName:PanelPopupWindow
*Description:TODO
*Date:2019/10/26 2:04
*/
@ccclass
export default class PanelPopupWindow extends UIView {

    protected static prefabUrl = "prefabs/panels/common/PanelPopupWindow";
    protected static className = "PanelPopupWindow";

    @property(cc.Label)
    public lab_content:cc.Label = null;

    private okAction:Function = null;
    private cancelAction:Function = null;

    onLoad() {
        super.onLoad();
    }

    public Show(msg: UIViewPanelPopupWindowMsg = null) {
        super.Show();
        if (msg == null) {
            return;
        }
        this.okAction =  msg.okAction;
        this.cancelAction = msg.cancelAction;
        this.lab_content.string = msg.content;
    }

    public Hide() {
        super.Hide();
        this.okAction = null;
        this.cancelAction = null;
    }

    protected onEnable(): void {
        super.onEnable();
    }

    protected onDisable(): void {
        super.onDisable();
    }

    public OnBtnClose() {
        this.Hide();
    }
}