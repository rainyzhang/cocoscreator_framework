const {ccclass, property} = cc._decorator;
/*
*ClassName:GameListItem
*Description:TODO
*Date:2019/10/30 21:55
*/
@ccclass
export default class GameListItem extends cc.Component {

    @property(cc.Label) label: cc.Label = null;

    index: number = 0

    getIndex(): number {
        return this.index
    }

    init(index: number) {
        let self = this;
        self.label.string = `${index}`
        self.index = index
    }

    itemShow() {
        this.node.opacity = 255
    }

    itemHide() {
        this.node.opacity = 0
    }
}