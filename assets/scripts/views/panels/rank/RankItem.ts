import UIView from "../../../core/mvcFrame/view/UIView";
import { Model } from "../../../core/mvcFrame/Model";
import { NotifyViewType } from "../../../core/mvcFrame/view/UIBaseView";
import RankModel from "../../../model/RankModel";
import { stringFormat } from "../../../utils/Utils";
import BaseCellComponent from "../../../core/common/scrollview/BaseCellComponent";
import ResManager from "../../../core/manager/ResManager";
import { CCLabel, CCSprite } from "../../../core/base/CCComponent";


const { ccclass, property } = cc._decorator;
/*
*ClassName:RankItem
*Description:TODO
*Date:2019/10/26 13:53
*/
@ccclass
export default class RankItem extends BaseCellComponent {

    @property(CCLabel)
    lab_rank: CCLabel = null;
    @property(CCLabel)
    lab_nickname: CCLabel = null;
    @property(CCLabel)
    lab_score: CCLabel = null;

    @property(CCSprite)
    img_head: CCSprite= null;
    @property(CCSprite)
    img_rank: CCSprite = null;

    private RankModel: RankModel = null;

    public UpdateViewModel(model: Model, index: number) {
        super.UpdateViewModel(model, index);
        this.RankModel = <RankModel>model;

        this.updateModel();
    }

    private updateModel() {
        if (this.RankModel.ranking < 4) {
            this.img_rank.node.active = true;
            this.lab_rank.node.active = false;
            ResManager.instance.onGetSpriteFromAtla("textures/atlas/panel_rank", 'img_rank_' + this.RankModel.ranking, this.img_rank);
        } else {
            this.img_rank.node.active = false;
            this.lab_rank.node.active = true;
            this.lab_rank.string = stringFormat("{0}", this.RankModel.ranking);
        }
        this.lab_score.string = this.RankModel.score.toString();
        this.lab_nickname.string = this.RankModel.nickname;
        ResManager.instance.onLoadImage(this.img_head, this.RankModel.avatarUrl);
    }

    protected onEnable() {


    }

    protected onDisable(): void {

    }


}