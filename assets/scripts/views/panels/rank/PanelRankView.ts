import UIView from "../../../core/mvcFrame/view/UIView";
import {UIViewPanelPopupWindowMsg} from "../../../eventMsg/AllEventMsg";
import ObjectPool from "../../../core/common/ObjectPool";
import RankController from "../../../controller/rank/RankController";
import RankModel from "../../../model/RankModel";
import {NotifyViewType} from "../../../core/mvcFrame/view/UIBaseView";
import RankScrollViewComponent from "./RankScrollViewComponent";

const {ccclass, property} = cc._decorator;
/*
*ClassName:PanelRankView
*Description:TODO
*Date:2019/10/26 14:13
*/
@ccclass
export default class PanelRankView extends UIView {

    protected static prefabUrl = "prefabs/panels/rank/PanelRankView";
    protected static className = "PanelRankView";

    @property(RankScrollViewComponent)
    RankScrollViewComponent: RankScrollViewComponent = null;


    @property(cc.Prefab)
    public ItemPrefab: cc.Prefab = null;
    @property(cc.Node)
    public ItemPrefabParent: cc.Node = null;

    private RankItem_objPool: ObjectPool<RankController> = null;

    onLoad() {
        super.onLoad();

        // this.RankItem_objPool = new ObjectPool<RankController>();
        // this.RankItem_objPool.InitPool(this.ItemPrefab, this.ItemPrefabParent);
    }

    private testModel: RankModel = null;

    public Show(msg: UIViewPanelPopupWindowMsg = null) {
        super.Show();
        // if (msg == null) {
        //     return;
        // }
        // let id: string = "1"
        // if (!RankModel.GetAllList().some((item)=> {return item.id == id})) {
        //     this.testModel = new RankModel();
        //     this.testModel.ranking = 2;
        //     this.testModel.nickname = "Rainy Zhang";
        //     this.testModel.Id = id;
        //     let ctrl: RankController = this.RankItem_objPool.GetElement<RankController>();
        //     ctrl.BindModel(this.testModel);
        //     this.testModel.NotifyChange(NotifyViewType.show);
        // } else {
        //     let model: RankModel = RankModel.GetAllList().find((item) => item.id == id);
        //     model.ranking = 1;
        //     model.nickname = "赵云";
        //     model.NotifyChange(NotifyViewType.update);
        // }
        let url = "https://wx.qlogo.cn/mmopen/vi_32/XwGs9nSHL3os2VLBeVzu9ibK6gq8h2DqIGGamMwBLHX2OwiaD0dibRBTZ1apnsa630VkPFZcMDlN5eNTUldXS1z2Q/132";



        // let s :any= {};
        // s.ranking = 1;
        // s.nickname = "nickname" + 0;
        // s.avatarUrl = url;
        // s.score = 100 - 1;
        //
        // let model:RankModel = null;
        //
        // for (let i = 0; i < 100; i++) {
        //     if (!RankModel.GetAllList().some((item)=> {return item.id == i.toString()})) {
        //         model = new RankModel(s);
        //         model.id = (i).toString();
        //     } else {
        //         model = RankModel.GetAllList().find((item) => item.id == i.toString());
        //     }
        // }
        //
        // this.RankScrollViewComponent.onInitCellDataList(RankModel.GetAllList());

        let model:RankModel = null;
        //let url = "https://wx.qlogo.cn/mmopen/vi_32/XwGs9nSHL3os2VLBeVzu9ibK6gq8h2DqIGGamMwBLHX2OwiaD0dibRBTZ1apnsa630VkPFZcMDlN5eNTUldXS1z2Q/132";
        for (let i = 0; i < 100; i++) {
            if (!RankModel.GetAllList().some((item)=> {return item.id == i.toString()})) {
                model = new RankModel();
                model.ranking = i + 1;
                model.nickname = "nickname" + i;
                model.avatarUrl = url;
                model.score = 100 - i;
                model.id = i.toString();
            } else {
                model = RankModel.GetAllList().find((item) => item.id == i.toString());
                model.ranking = i + 1;
                model.avatarUrl = url;
                model.nickname =  "nickname" + i;
                model.score = 100 - i;
            }
        }

        this.RankScrollViewComponent.onInitCellDataList(RankModel.GetAllList());
    }

    public Hide() {
        super.Hide();

    }

    protected onEnable(): void {
        super.onEnable();
    }

    protected onDisable(): void {
        super.onDisable();
    }

    public OnBtnClose() {
        this.Hide();
    }
}