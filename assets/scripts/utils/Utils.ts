/*
*ClassName:Utils
*Description:常用工具类
*Date:2019/10/23 0:03
*/
export function stringFormat(...args) {
    let formatStr = arguments[0];
    if (typeof formatStr === 'string') {
        let pattern,
            length = arguments.length;

        for (let i = 1; i < length; i++) {
            pattern = new RegExp('\\{' + (i - 1) + '\\}', 'g');
            formatStr = formatStr.replace(pattern, arguments[i]);
        }
    } else {
        formatStr = '';
    }

    return formatStr;
}

export default class Utils {

    /**遍历执行方法*/
    public static ReverseIterateList<T>(list: T[], action: Function) {
        for (let i = list.length - 1; i >= 0; i--)
            action(list [i]);
    }


}