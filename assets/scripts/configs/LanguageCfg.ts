﻿/*
* 此类由rainy自动生成
* LanguageCfg
*/
export default class LanguageCfg {
        public mDataMap:{ [key:string] :LanguageCfgData };

        public constructor(map: Object) {
              this.mDataMap = {};
              for (let key in map) {
                  this.mDataMap[key] = new LanguageCfgData(map[key]);
              }
        }
}

export class LanguageCfgData {
        //语言id
        public keyId: number;
        //中文提示
        public msg_cn: string;

        public constructor(data: Object) {
               for (let i in data) {
                   this[i] = data[i];
              }
        }
}
