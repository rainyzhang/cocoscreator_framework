﻿/*
* 此类由rainy自动生成
* GameCfg
*/
export default class GameCfg {
        public mDataList: GameCfgData[];

        public constructor(list: Object[]) {
               this.mDataList = [];
               for (let i in list) {
                   this.mDataList.push(new GameCfgData(list[i]));
              }
        }
}

export class GameCfgData {
        //游戏名称
        public game_name: string;
        //日志
        public game_log_level: number;
        //游戏连接网络
        public game_net_type: number;
        //测试debug
        public game_url_debug: string;
        //测试体验服地址
        public game_url_debug_test: string;
        //测试正式服地址
        public game_url_release: string;

        public constructor(data: Object) {
               for (let i in data) {
                   this[i] = data[i];
              }
        }
}
