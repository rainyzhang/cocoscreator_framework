
/*
*ClassName:ConfigController
*Description:TODO
*Date:2019/11/7 23:03
*/

import GameCfg, {GameCfgData} from "../configs/GameCfg";
import MgrCenter from "../MgrCenter";
import LanguageCfg from "../configs/LanguageCfg";
import { CCSingleton } from "../core/base/CCComponent";
import { ConfigManager } from "../manager/ConfigManager";

export class ConfigController {

    private _gameConfig: GameCfgData;
    private _languageCfgMap: LanguageCfg;

    /***初始化配置表*/
    public initConfig() {
        if (this._gameConfig != null) {
            return;
        }

        let config = CCSingleton.getInstance(ConfigManager).config;
        this._gameConfig = new GameCfg(config.GameCfg).mDataList[0];
        this._languageCfgMap = new LanguageCfg(config.LanguageCfg);
    }

    /**
     * 获取游戏配置表
     */
    public get gameConfig(): GameCfgData {
        return this._gameConfig;
    }

    /***获取错误码的msg*/
    public getLanguageCfgMsg(errorCode: any): string {
        if (this._languageCfgMap.mDataMap[errorCode]) {
            return this._languageCfgMap.mDataMap[errorCode].msg_cn;
        }
        return errorCode + '';
    }

    // constructor() {

    // }

    private static _instance: ConfigController = null;
    public static get instance() {
        if (!this._instance) {
            this._instance = new ConfigController();
        }
        return this._instance;
    }
}