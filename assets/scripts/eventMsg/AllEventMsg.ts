
/*
*ClassName:AllEventMsg
*Description:TODO
*Date:2019/10/8 13:39
*/

import {BaseEventMessage} from "../core/mvcFrame/TMessage";

export default class AllEventMsg  {


}


export class LoginViewMsg extends BaseEventMessage{
    protected static className = "LoginViewMsg";

    public msg:string = "open login";
}
export class UIViewMsg extends BaseEventMessage{
    protected static className = "UIViewMsg";

    public msg:string = "UIViewMsg OPEN";
}
//------------------------------------------//
export class UIViewSceneLoginMsg extends BaseEventMessage{
    protected static className = "UIViewSceneLoginMsg";

    public msg:BaseEventMessage = null;
    public uiClass:any;

    public sceneName:string = "SceneLogin";
}

export class UIViewSceneGameMsg extends BaseEventMessage{
    protected static className = "UIViewSceneGameMsg";

    public msg:BaseEventMessage = null;
    public uiClass:any;

    public sceneName:string = "SceneGame";
}
//------------------------------------------//
/***错误弹窗提示*/
export class UIViewPanelPopupWindowMsg extends BaseEventMessage{
    protected static className = "UIViewPanelPopupWindowMsg";

    public content:string = "网络不佳";

    public okAction:Function = null;
    public cancelAction :Function = null;
}

export class UIViewPanelRankViewMsg extends BaseEventMessage{
    protected static className = "UIViewPanelRankViewMsg";

}

export class UIViewPanelGameListViewMsg extends BaseEventMessage{
    protected static className = "UIViewPanelGameListViewMsg";

}

export class UIViewPanelWheelLuckyViewMsg extends BaseEventMessage{
    protected static className = "UIViewPanelWheelLuckyViewMsg";

}

export class UIViewPanelSettingMsg extends BaseEventMessage{
    protected static className = "UIViewPanelSettingMsg";

}
