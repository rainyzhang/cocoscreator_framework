/*
*ClassName:ConfigManager
*Description:TODO
*Date:2019/10/27 12:48
*/

import ResManager from "../core/manager/ResManager";

export class ConfigManager {

    private _config: any = {};//加载对象存储
    public get config() {
        return this._config;
    }
    /**
     * 加载文件夹下的所有的配置表文件
     * @returns {Promise<any>}
     */
    public onLoadAllConfig(): Promise<any> {
        return new Promise((resolve, reject) => {
            ResManager.instance.onLoadDirAllJson("configs").then((res: cc.JsonAsset[]) => {
                this._config = {};
                res.forEach((item: cc.JsonAsset) => {
                    for(let key in item.json){
                        for(let i in item.json[key]){
                            this._config[i] = item.json[key][i];
                        }
                    }
                })
                console.log("配置表加载完成", this._config);
                resolve();
            }).catch((error: Error) => {
                console.error("配置表加载失败", error.message);
                reject(error);
            })
        });
    }

    private InitConfig(){

    }

}