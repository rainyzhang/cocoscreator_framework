
/*
*ClassName:WebManager
*Description:TODO
*Date:2019/11/7 23:17
*/

export default class WebManager  {


    private _url: string = "";
    private _serverName: string = "";

    private static _instance: WebManager = null;
    public static get instance(): WebManager {
        if (!this._instance) {
            this._instance = new WebManager();
        }
        return this._instance;
    }
}