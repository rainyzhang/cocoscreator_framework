import UIViewCollection from "../core/mvcFrame/view/UIViewCollection";
import {LoginViewMsg} from "../eventMsg/AllEventMsg";
import TMessage, {
    BaseEventMessage,
    UIViewPanelHideAllMsg,
    UIViewPanelLoadingMsg, UIViewSceneLoadingMsg,
    UIViewSceneShowMsg
} from "../core/mvcFrame/TMessage";

import UIBaseView, {UIClass} from "../core/mvcFrame/view/UIBaseView";
import ResMgr from "../core/manager/ResManager";
import LoaderItem from "../core/loader/LoaderItem";
import UIView from "../core/mvcFrame/view/UIView";
import SceneLogin from "../views/scenes/SceneLogin";
import url = cc.url;

const {ccclass, property} = cc._decorator;
/*
*ClassName:SceneManager
*Description:TODO
*Date:2019/10/25 22:47
*/
@ccclass
export default class SceneManager extends UIViewCollection {

    public static instance:SceneManager = null;

    @property(cc.Node)
    public Parent:cc.Node = null;

    onLoad() {
        SceneManager.instance = this;

        TMessage.AddListener(UIViewSceneShowMsg,this.ShowMsg,this);
        TMessage.AddListener(UIViewSceneLoadingMsg,this.SceneLoadingMsg,this);

        TMessage.NotifyShowScene(SceneLogin);
    }

    private SceneLoadingMsg(msg: UIViewSceneLoadingMsg) {
        this.ShowUI(msg.prefabPath, msg.msg, msg.onProgress, msg.onComplete,msg.isShow);
    }


    /***场景切换显示*/
    private ShowMsg = (msg:UIViewSceneShowMsg)=>{
        if(msg.uiClass.getUrl() == null){
            console.error("请添加scene url",msg.uiClass);
            return;
        }
        this.ShowUI(msg.uiClass.getUrl(),msg.msg,msg.onProgress,msg.onComplete);
    }

    //public ShowUI<T extends UIBaseView>(uiClass:UIClass<T>, baseEventMessage :BaseEventMessage= null, onProgress = null, onComplete = null){
    public ShowUI(prefabPath:string, baseEventMessage :BaseEventMessage= null, onProgress = null, onComplete = null,isShow:boolean = true){
        if (!onComplete) {
            onComplete = onProgress;
            onProgress = null;
        }

        //let prefabPath = uiClass.getUrl();
        console.log("scene prefabPath ================ ",prefabPath);
        let loadTime: any = new Date();
        let scriptName: string = null;
        // console.error("childrenCount = ", this.panelNode.childrenCount);
        let arr: string[] = prefabPath.split('/');
        if (arr.length > 0) {
            scriptName = arr[arr.length - 1];
        }
        if (scriptName == null) {
            // UIMgr.instance.showConfirm(msg, () => {
            //     this.showUI(prefabPath, params, onProgress, onComplete, isPreload);
            // })
            // Debug.error("加载路径的名字为null = ", prefabPath);
            return;
        }

        if(this.IsUIView(scriptName)){
            TMessage.NotifyMessage(UIViewPanelHideAllMsg);
            this.ShowOneUIView(scriptName,baseEventMessage,true);
            return;
        }

        let resMgr: ResMgr = ResMgr.instance;
        let loaderItem = new LoaderItem(prefabPath, cc.Prefab, 3);
        resMgr.addLoadItem(prefabPath, loaderItem);
        loaderItem.load((res: cc.Prefab[]) => {
            let prefab = res[0];
            if (!prefab) {
                resMgr.removeLoadItem(prefabPath);
                console.error("找不到预制体_path :" + prefabPath);
                return;
            }
            let uiNode = cc.instantiate(prefab);
            uiNode.setContentSize(cc.winSize);
            uiNode.setPosition(0, 0);
            //uiNode.addComponent(ButtonComponent);
            // let obj = uiNode.getComponent(scriptName);
            let obj: UIView = <UIView>uiNode.getComponent(scriptName);
            if (obj == null) {
                // UIMgr.instance.showConfirm(msg, () => {
                //     this.showUI(prefabPath, params, onProgress, onComplete, isPreload);
                // })
                return;
            }
            obj.initLoader(prefabPath, resMgr,SceneManager.instance);
            //obj.Show(baseEventMessage);
            let loadFishedTime: any = new Date();
            console.log("加载scene", scriptName, "Prefab消耗:", +(loadFishedTime - loadTime) + "毫秒");

            this.AddView(scriptName,obj);

            TMessage.NotifyMessage(UIViewPanelHideAllMsg);
            this.ShowOneUIView(scriptName,baseEventMessage,true);

            onComplete && onComplete(obj);

        }, (error: Error) => {
            console.error('游戏异常,启动失败1231' + error.message);
            // UIMgr.instance.showConfirm(msg, () => {
            //     this.showUI(prefabPath, params, onProgress, onComplete, isPreload);
            // })
            resMgr.removeLoadItem(prefabPath);

        }, onProgress, () => {
            // console.error("加载失败222222");
        })
    }
}