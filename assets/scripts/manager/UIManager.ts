import UIViewCollection from "../core/mvcFrame/view/UIViewCollection";
import UIBaseView, {UIClass} from "../core/mvcFrame/view/UIBaseView";
import TMessage, {
    BaseEventMessage,
    UIViewPanelHideAllMsg,
    UIViewPanelLoadingMsg,
    UIViewPanelShowMsg
} from "../core/mvcFrame/TMessage";
import ResMgr from "../core/manager/ResManager";
import LoaderItem from "../core/loader/LoaderItem";
import UIView from "../core/mvcFrame/view/UIView";
import {LoginViewMsg} from "../eventMsg/AllEventMsg";
import LoadingSceneComponent from "../core/components/LoadingSceneComponent";

const {ccclass, property} = cc._decorator;
/*
*ClassName:UIManager
*Description:TODO
*Date:2019/10/25 12:55
*/
@ccclass
export default class UIManager extends UIViewCollection {

    public static instance: UIManager = null;

    @property(cc.Node)
    public Parent: cc.Node = null;

    @property(cc.Node)
    public ExtraParent: cc.Node = null;

    private loadingSceneComponent: LoadingSceneComponent;


    onLoad() {
        UIManager.instance = this;

        // let msg:LoginViewMsg = new LoginViewMsg();
        // msg.msg ="open login";
        TMessage.AddListener(UIViewPanelShowMsg, this.ShowMsg, this);
        TMessage.AddListener(UIViewPanelHideAllMsg, this.HideAllMsg, this);
        TMessage.AddListener(UIViewPanelLoadingMsg, this.PanelLoadingMsg, this);


        //TMessage.NotifyShowUI(LoginView,msg);
    }

    //关闭所有UI
    protected HideAllMsg(msg: UIViewPanelHideAllMsg) {
        this.HideAllUIView();
    }

    private PanelLoadingMsg(msg: UIViewPanelLoadingMsg) {
        this.ShowUI(msg.prefabPath, msg.msg, msg.onProgress, msg.onComplete,msg.isShow);
    }

    public showLoadingScene(prefabPath: string[] | string, onComplete = null) {
        if (this.loadingSceneComponent != null) {
            this.loadingSceneComponent.showLoadingScene(prefabPath, onComplete)
            return;
        }
        let res: ResMgr = ResMgr.instance;
        let tempNode: cc.Node;
        res.onLoadPrefab("prefabs/components/common/LoadingSceneComponent").then((res: cc.Prefab) => {
            tempNode = cc.instantiate(res);
            tempNode.setParent(this.ExtraParent);
            this.loadingSceneComponent = tempNode.getComponent(LoadingSceneComponent);
            this.loadingSceneComponent.showLoadingScene(prefabPath, onComplete)
        })

    }

    //打开一个UI
    private ShowMsg(msg: UIViewPanelShowMsg) {
        if (msg.uiClass.getUrl() == null) {
            console.error("请添加scene url", msg.uiClass);
            return;
        }
        this.ShowUI(msg.uiClass.getUrl(), msg.msg, msg.onProgress, msg.onComplete);
        //this.ShowUI(msg.uiClass,msg.msg,msg.onProgress,msg.onComplete);
    }

    //public ShowUI<T extends UIBaseView>(uiClass:UIClass<T>, baseEventMessage :BaseEventMessage= null, onProgress = null, onComplete = null){
    public ShowUI(prefabPath: string, baseEventMessage: BaseEventMessage = null, onProgress = null, onComplete = null,isShow :boolean = true) {
        if (!onComplete) {
            onComplete = onProgress;
            onProgress = null;
        }

        // let prefabPath = uiClass.getUrl();
        console.log("panel prefabPath ===== ", prefabPath);

        let loadTime: any = new Date();
        // console.log("open_path:", prefabPath);
        let scriptName: string = null;
        // console.error("childrenCount = ", this.panelNode.childrenCount);
        let arr: string[] = prefabPath.split('/');
        if (arr.length > 0) {
            scriptName = arr[arr.length - 1];
        }
        if (scriptName == null) {
            // UIMgr.instance.showConfirm(msg, () => {
            //     this.showUI(prefabPath, params, onProgress, onComplete, isPreload);
            // })
            // Debug.error("加载路径的名字为null = ", prefabPath);
            return;
        }

        if (this.IsUIView(scriptName)) {
            this.ShowPanel(scriptName, baseEventMessage,isShow);
            return;
        }

        let resMgr: ResMgr = ResMgr.instance;
        let loaderItem = new LoaderItem(prefabPath, cc.Prefab, 3);
        resMgr.addLoadItem(prefabPath, loaderItem);
        loaderItem.load((res: cc.Prefab[]) => {
            let prefab = res[0];
            if (!prefab) {
                resMgr.removeLoadItem(prefabPath);
                console.error("找不到预制体_path :" + prefabPath);
                return;
            }
            let uiNode = cc.instantiate(prefab);
            uiNode.setContentSize(cc.winSize);
            uiNode.setPosition(0, 0);
            //uiNode.addComponent(ButtonComponent);
            // let obj = uiNode.getComponent(scriptName);
            let obj: UIView = <UIView>uiNode.getComponent(scriptName);
            if (obj == null) {
                // UIMgr.instance.showConfirm(msg, () => {
                //     this.showUI(prefabPath, params, onProgress, onComplete, isPreload);
                // })
                return;
            }
            obj.initLoader(prefabPath, resMgr, UIManager.instance);
            if(isShow){
                obj.Show(baseEventMessage);
            }
            let loadFishedTime: any = new Date();
            console.log("加载Panel", scriptName, "Prefab消耗:", +(loadFishedTime - loadTime) + "毫秒");

            this.AddView(scriptName, obj);

            onComplete && onComplete(obj);
        }, (error: Error) => {
            console.error('游戏异常,启动失败1231' + error.message);
            // UIMgr.instance.showConfirm(msg, () => {
            //     this.showUI(prefabPath, params, onProgress, onComplete, isPreload);
            // })
            resMgr.removeLoadItem(prefabPath);

        }, onProgress, () => {
            // console.error("加载失败222222");
        })
    }
}