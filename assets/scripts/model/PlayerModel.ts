import {Model} from "../core/mvcFrame/Model";

/*
*ClassName:PlayerModel
*Description:TODO
*Date:2019/10/24 23:13
*/
export default class PlayerModel extends Model {

    //public modelClassName:string;
    // public get getClassName(): string {
    //     return "PlayerModel";
    // }

    public static className = "PlayerModel";

    public constructor() {
        super("PlayerModel");
    }

    public static GetAllList():PlayerModel[]  {
        return Model.GetAllModelList(PlayerModel);
    }

}