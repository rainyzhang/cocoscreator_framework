/*
*ClassName:RankModel
*Description:TODO
*Date:2019/10/26 15:35
*/

import {Model} from "../core/mvcFrame/Model";

export default class RankModel extends Model {

    public static className = "RankModel";

    public ranking: number;
    public nickname: string;
    public avatarUrl:string;
    public score:number;

    // public constructor() {
    //     super("RankModel");
    // }

    public constructor(data?: Object) {
        super("RankModel",data);
    }


    public static GetAllList(): RankModel[] {
        return Model.GetAllModelList(RankModel);
    }
}