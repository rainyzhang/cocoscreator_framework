
/*
*ClassName:GameService
*Description:TODO
*Date:2019/11/7 23:42
*/

export default class GameService  {

    private _url: string = "";
    private _serverName: string = "";

    constructor(_serverName: string, url: string) {
        this._serverName = _serverName;
        this._url = url;
        console.log(this._serverName + '/' + this._url);
    }
}