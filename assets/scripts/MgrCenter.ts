import TMessage from "./core/mvcFrame/TMessage";
import SceneLogin from "./views/scenes/SceneLogin";
import { ConfigController } from "./controller/ConfigController";
import { CCSingleton } from "./core/base/CCComponent";
import { ConfigManager } from "./manager/ConfigManager";


const {ccclass, property} = cc._decorator;

@ccclass
export default class MgrCenter extends cc.Component {

    onLoad () {
        console.log('游戏启动');
        this.onLoadSceneLogin();
    }

    /**
    /* 加载配置文件以及进入开始界面
    /*/
    private async loadNeedConfig() {
        //加载游戏配置
        await CCSingleton.getInstance(ConfigManager).onLoadAllConfig();
    }

    //打开SceneLogin界面
    private onLoadSceneLogin() {
        this.loadNeedConfig().then(() => {

            CCSingleton.getInstance(ConfigController).initConfig();
            TMessage.NotifyShowUI(SceneLogin,null,null,()=>{

            })
        }).catch((error: Error) => {
            // MgrCenter.UIManager.showConfirm(MgrCenter.ConfigManager.getMsgByCodeConfig(error.message), () => {
            //     this.onLoadSceneLogin();
            // })
        });
    }

}
