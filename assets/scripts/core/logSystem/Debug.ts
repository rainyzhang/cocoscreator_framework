/**
 * 日志等级枚举
 */

export enum LogLevel {
    CLOSE = -1,
    ERROR = 0,
    WARN = 1,
    NET = 2,
    INFO = 3,
    DEBUG = 4,
}

/*
*ClassName:Debug
*Description:日志系统
*Date:2019/10/7 13:47
*/
export default class Debug {

    /**日志等级*/
    private static _level: number = 0;//-1关闭日志 0-开启错误日志 1-开启警告日志 2-网络日志 3-输出日志 4-调试日志

    public static netToggle: boolean = true;//网络开关

    /**设置日志等级*/
    public static SetLevel(level: LogLevel) {
        this._level = level;
    }

    public static getDateString() {
        let d = new Date();
        let myYear = d.getFullYear();
        let myMonth = d.getMonth() + 1;
        let myDay = d.getDate();
        let str = (d.getHours()).toString();
        let timeStr = "";

        timeStr += myYear + "-";
        timeStr += (myMonth < 10 ? "0" + myMonth : myMonth ) + "-";
        timeStr += (myDay < 10 ? "0" + myDay : myDay) + " ";
        timeStr += (parseInt(str) < 10 ? "0" + str : str) + ":";
        str = "" + d.getMinutes();
        timeStr += (parseInt(str) < 10 ? "0" + str : str) + ":";
        str = "" + d.getSeconds();
        timeStr += (parseInt(str) < 10 ? "0" + str : str) + ":";
        str = "" + d.getMilliseconds();
        if (str.length == 1) str = "00" + str;
        if (str.length == 2) str = "0" + str;
        timeStr += str;
        timeStr = "[" + timeStr + "]";
        return timeStr;
    }

    static stack(index) {
        let e = new Error();
        let lines = e.stack.split("\n");
        lines.shift();
        let result = [];
        lines.forEach(function (line) {
            line = line.substring(7);
            let lineBreak = line.split(" ");
            if (lineBreak.length < 2) {
                result.push(lineBreak[0]);
            } else {
                result.push({[lineBreak[0]]: lineBreak[1]});
            }
        });

        let list = [];
        if (index < result.length - 1) {
            for (let a in result[index]) {
                list.push(a);
            }
        }

        let splitList = list[0].split(".");
        return (splitList[0] + ".js->" + splitList[1] + ": ");
    }

    /**调试日志*/
    public static log = function (...args) {
        let backLog = console.log || cc.log || Debug.log;

        if (this._level >= 3) {
            backLog.call(this, "%s%s" + cc.js.formatStr.apply(cc, arguments), this.stack(2), Debug.getDateString());
        }

    };
    /**输出日志*/
    public static info = function (...args) {
        let backLog = console.log || cc.log || Debug.log;
        if (this._level >= 2) {
            backLog.call(this, "%c%s:" + cc.js.formatStr.apply(cc, arguments), "color:#00CD00;", Debug.getDateString());
        }
    };
    /**警告日志*/
    public static warn = function (...args) {
        let backLog = console.log || cc.log || Debug.log;
        if (this._level >= 1) {
            backLog.call(this, "%c%s:" + cc.js.formatStr.apply(cc, arguments), "color:#FFA100;", Debug.getDateString());
        }
    };

    /**网络日志*/
    public static netLog = function (...args) {
        let backLog = console.log || cc.log || Debug.log;
        if (!this.netToggle) {
            return;
        }
        if (this._level >= 2) {
            backLog.call(this, "%c%s:" + cc.js.formatStr.apply(cc, arguments), "color:#003FFF;", Debug.getDateString());
        }
    };
    /**错误日志*/
    public static error = function (...args) {
        let backLog = console.log || cc.log || Debug.log;
        if (this._level >= 0) {
            backLog.call(this, "%c%s:" + cc.js.formatStr.apply(cc, arguments), "color:red", Debug.getDateString());
        }
    };

}