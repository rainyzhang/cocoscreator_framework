/***基础类*/
const {ccclass, property} = cc._decorator;
//
@ccclass
export class CCComponent extends cc.Component { }

export class CCSingleton {
    private static instances: Map<{ new() }, Object> = new Map<{ new() }, Object>();

    public static getInstance<T>(classT: { new(): T }): T {
        if (!CCSingleton.instances.has(classT)) {
            let obj = new classT();
            CCSingleton.instances.set(classT, obj);
            return obj;
        }
        return <T>CCSingleton.instances.get(classT);
    }

    constructor() {
     
    }

}

export class CCAction extends cc.Action { }

export class CCAnimation extends cc.Animation { }

export class CCAnimationClip extends cc.AnimationClip { }

export class CCAnimationState extends cc.AnimationState { }

export class CCAsset extends cc.Asset { }

export class CCAudioSource extends cc.AudioSource { }

export class CCBitmapFont extends cc.BitmapFont { }

export class CCBlockInputEvents extends cc.BlockInputEvents { }

export class CCBoxCollider extends cc.BoxCollider { }

export class CCButton extends cc.Button { }

export class CCBBufferAsset extends cc.BufferAsset { }

export class CCCamera extends cc.Camera { }

export class CCCanvas extends cc.Canvas { }

export class CCCircleCollider extends cc.CircleCollider { }

export class CCCollider extends cc.Collider { }

export class CCCollisionManager extends cc.CollisionManager { }

export class CCColor extends cc.Color { }

// export class CCComponent extends cc.Component { }

export class CCContainerStrategy extends cc.ContainerStrategy { }

export class CCContentStrategy extends cc.ContentStrategy { }

// export class CCDetails extends cc.Details { }//有问题

export class CCDirector extends cc.Director { }

export class CCDistanceJoint extends cc.DistanceJoint { }

// export class CCDynamicAtlasManager extends cc.DynamicAtlasManager { }//有问题

export class CCEditBox extends cc.EditBox { }

// export class CCEqualToFrame extends cc.EqualToFrame { }//有问题

// export class CCEqualToWindow extends cc.EqualToWindow { }//有问题

export class CCEvent extends cc.Event { }

export class CCEventTarget extends cc.EventTarget { }

export class CCFiniteTimeAction extends cc.FiniteTimeAction { }

export class CCFont extends cc.Font { }

// export class CCGame extends cc.Game { }//有问题

export class CCGraphics extends cc.Graphics { }

// export class CCIntersection extends cc.Intersection { }//有问题

export function CCInstantiate(original: cc.Prefab): cc.Node {
    return cc.instantiate(original);
}

export function CCIinstantiate<T>(original: T): T {
    return cc.instantiate<T>(original);
}

export function CCIsValid(value: any, strictMode?: boolean): boolean {
    return cc.isValid(value, strictMode);
}
// /***关节类的基类*/
export class CCJoint extends cc.Joint { }

export class CCJsonAsset extends cc.JsonAsset { }

export class CCLabel extends cc.Label { }

export class CCLabelAtlas extends cc.LabelAtlas { }

export class CCLabelOutline extends cc.LabelOutline { }

export class CCLabelShadow extends cc.LabelShadow { }

export class CCLayout extends cc.Layout { }

export class CCLight extends cc.Light { }

export class CCLoadingItems extends cc.LoadingItems { }

// export class CCManifold extends cc.Manifold { }//有问题

// export class CCManifoldPoint extends cc.ManifoldPoint { }//有问题

export class CCMask extends cc.Mask { }

export class CCMat4 extends cc.Mat4 { }

export class CCMotionStreak extends cc.MotionStreak { }

export class CCMotorJoint extends cc.MotorJoint { }

export class CCMouseJoint extends cc.MouseJoint { }

// export class CCmacro extends cc.macro { }//有问题

// export class CCmisc extends cc.misc { }

export class CCNode extends cc.Node { }

export class CCNodePool extends cc.NodePool { }

export class CCObject extends cc.Object { }

// // export class CCOriginalContainer extends cc.OriginalContainer { }//有问题

export class CCPageView extends cc.PageView { }

export class CCPageViewIndicator extends cc.PageViewIndicator { }

export class CCParticleAsset extends cc.ParticleAsset { }

export class CCParticleSystem extends cc.ParticleSystem { }

export class CCPhysicsBoxCollider extends cc.PhysicsBoxCollider { }

export class CCPhysicsChainCollider extends cc.PhysicsChainCollider { }

export class CCPhysicsCircleCollider extends cc.PhysicsCircleCollider { }

export class CCPhysicsCollider extends cc.PhysicsCollider { }

export class CCPhysicsContact extends cc.PhysicsContact { }

// export class CCPhysicsImpulse extends cc.PhysicsImpulse { }//有问题

export class CCPhysicsManager extends cc.PhysicsManager { }

export class CCPhysicsPolygonCollider extends cc.PhysicsPolygonCollider { }

// export class CCPhysicsRayCastResult extends cc.PhysicsRayCastResult { }//有问题

// // export class CCPipeline extends cc.Pipeline { }//有问题

// // export class CCPlayable extends cc.Playable { }//有问题

export class CCPolygonCollider extends cc.PolygonCollider { }

export class CCPrefab extends cc.Prefab { }

export class CCPrismaticJoint extends cc.PrismaticJoint { }

export class CCPrivateNode extends cc.PrivateNode { }

export class CCProgressBar extends cc.ProgressBar { }

// // export class CCProportionalToFrame extends cc.ProportionalToFrame { }//有问题

// // export class CCProportionalToWindow extends cc.ProportionalToWindow { }//有问题

export class CCQuat extends cc.Quat { }

export class CCRawAsset extends cc.RawAsset { }

export class CCRect extends cc.Rect { }

export class CCRenderComponent extends cc.RenderComponent { }

export class CCResolutionPolicy extends cc.ResolutionPolicy { }

export class CCRevoluteJoint extends cc.RevoluteJoint { }

export class CCRichText extends cc.RichText { }

export class CCRigidBody extends cc.RigidBody { }

export class CCRopeJoint extends cc.RopeJoint { }

export class CCScene extends cc.Scene { }

export class CCSceneAsset extends cc.SceneAsset { }

export class CCScheduler extends cc.Scheduler { }

export class CCScrollbar extends cc.Scrollbar { }

export class CCScrollView extends cc.ScrollView { }

export class CCSize extends cc.Size { }

export class CCSkeletonAnimation extends cc.SkeletonAnimation { }

export class CCSkeletonAnimationClip extends cc.SkeletonAnimationClip { }

export class CCSkinnedMeshRenderer extends cc.SkinnedMeshRenderer { }

export class CCSlider extends cc.Slider { }

export class CCSprite extends cc.Sprite { }

export class CCSpriteAtlas extends cc.SpriteAtlas { }

export class CCSpriteFrame extends cc.SpriteFrame { }

export class CCSwanSubContextView extends cc.SwanSubContextView { }

export class CCSystemEvent extends cc.SystemEvent { }

export class CCTextAsset extends cc.TextAsset { }

export class CCTexture2D extends cc.Texture2D { }

export class CCTiledLayer extends cc.TiledLayer { }

export class CCTiledMap extends cc.TiledMap { }

export class CCTiledMapAsset extends cc.TiledMapAsset { }

export class CCTiledObjectGroup extends cc.TiledObjectGroup { }

export class CCTiledTile extends cc.TiledTile { }

export class CCToggle extends cc.Toggle { }

export class CCToggleContainer extends cc.ToggleContainer { }

export class CCTouch extends cc.Touch { }

export class CCTTFFont extends cc.TTFFont { }

export class CCTween extends cc.Tween { }

// export class CCTypeScript extends cc.TypeScript { }//有问题

export class CCVec2 extends cc.Vec2 { }

export class CCVec3 extends cc.Vec3 { }

export class CCVec4 extends cc.Vec4 { }

export class CCVideoPlayer extends cc.VideoPlayer { }

// export class CCView extends cc.View { }

export class CCViewGroup extends cc.ViewGroup { }

export function CCv2(x?: number | any, y?: number): cc.Vec2 {
    return cc.v2(x, y);
}

export function CCCview(): cc.View {
    return cc.view;
}

// export class CCvisibleRect extends cc.visibleRect { }

export class CCWebView extends cc.WebView { }

export class CCWeldJoint extends cc.WeldJoint { }

export class CCWheelJoint extends cc.WheelJoint { }

export class CCWidget extends cc.Widget { }

export class CCWXSubContextView extends cc.WXSubContextView { }

