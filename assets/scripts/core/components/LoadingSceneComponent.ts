
import TMessage from "../mvcFrame/TMessage";
import UIView from "../mvcFrame/view/UIView";
import {CCComponent} from "../base/CCComponent";

const {ccclass, property} = cc._decorator;
/*
*ClassName:LoadingSceneComponent
*Description:TODO
*Date:2019/6/2 20:38
*/
@ccclass
export default class LoadingSceneComponent extends CCComponent {

    @property(cc.Node)
    loadingSceneNode: cc.Node = null;
    @property(cc.ProgressBar)
    progressBar_one: cc.ProgressBar = null;
    @property(cc.ProgressBar)
    progressBar: cc.ProgressBar = null;
    @property(cc.Label)
    lab_asset: cc.Label = null;

    public showLoadingScene(prefabPath: string[] | string, onComplete ?: Function) {
        if (this.loadingSceneNode.active) {
            return;
        }
        this.loadingSceneNode.stopAllActions();
        this.loadingSceneNode.opacity = 255;
        this.loadingSceneNode.active = true;
        this.progressBar_one.progress = 0;
        this.progressBar.progress = 0;
        let urls: string[] = [];

        typeof prefabPath === "string" ? urls.push(prefabPath) : urls = prefabPath;
        //console.error('url = ', urls)
        let count = 0;
        let len = urls.length;
        let loadProgress = () => {
            let path = urls[count];
            if (path.indexOf('Panel') >= 0) {
                TMessage.NotifyLoadingUI(path,null,(completedCount: number,totalCount: number, item: any)=>{
                    this.progressBar_one.progress = completedCount / totalCount;
                },(res:UIView)=>{
                    res.Hide();
                    ++count;
                    this.lab_asset.string = path;
                    this.progressBar.progress = count / len;
                    if (count >= len) {
                        this.hideLoadingScene();
                        onComplete();
                    } else {
                        loadProgress();
                    }
                });
            } else if (path.indexOf('Scene') >= 0) {

                TMessage.NotifyLoadingScene(path,null,(completedCount: number,totalCount: number, item: any)=>{
                    this.progressBar_one.progress = completedCount / totalCount;
                },(res:UIView)=>{
                    ++count;
                    this.lab_asset.string = path;
                    this.progressBar.progress = count / len;
                    if (count >= len) {
                        this.hideLoadingScene();
                        onComplete();
                    } else {
                        loadProgress();
                    }
                });
            } else {
                console.error("error 还没写其他加载进度")
                //UIMgr.instance.showTip('还未写代码....')
            }
        }
        loadProgress();
    }

    hideLoadingScene() {

        this.loadingSceneNode.stopAllActions();
        this.loadingSceneNode.opacity = 255;
        let action1 = cc.delayTime(0.2);
        let action2 = cc.fadeOut(0.3);
        this.loadingSceneNode.runAction(cc.sequence(action1, action2, cc.callFunc(() => {
            this.loadingSceneNode.opacity = 0;
            this.loadingSceneNode.active = false;
        })));
    }

}