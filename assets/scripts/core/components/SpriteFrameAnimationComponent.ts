import {CCComponent, CCSprite, CCSpriteAtlas} from "../base/CCComponent";

const {ccclass, property} = cc._decorator;
/*
*ClassName:SpriteFrameAnimationComponent
*Description:TODO
*Date:2019/12/2 22:43
*/
@ccclass
export default class SpriteFrameAnimationComponent extends CCComponent {

    @property({type: cc.SpriteAtlas,tooltip:"图集"})
    game_spriteAtlas: cc.SpriteAtlas = null;

    /**
     * nameStr = 图集名称
     * count = 总帧数
     * intervalTime = 间隔
     * isLoop = 是否喜循环播放
     * */
    public playAni(nameString: string, count: number, intervalTime: number, isLoop: boolean = false) {
        if (this.game_spriteAtlas == null) {
            console.error("图集不能为空,请检测...");
            return;
        }
        this.stopAni();
        this.node.getComponent(cc.Sprite).spriteFrame = this.game_spriteAtlas.getSpriteFrame(nameString + 0);
        let array = [];
        for (let i = 0; i < count; i++) {
            array.push(cc.delayTime(intervalTime));
            array.push(cc.callFunc(() => {
                this.node.getComponent(cc.Sprite).spriteFrame = this.game_spriteAtlas.getSpriteFrame(nameString + i);
            }));
        }

        if (isLoop) {
            this.node.runAction(cc.repeatForever(cc.sequence(array)));
        } else {
            this.node.runAction(cc.sequence(array));
        }
    }

    /***停止动画*/
    public stopAni() {
        this.node.stopAllActions();
    }

}