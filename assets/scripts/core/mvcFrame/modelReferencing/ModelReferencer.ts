/*
*ClassName:ModelReferencer
*Description:TODO
*Date:2019/10/8 0:36
*/

import {Model} from "../Model";

export abstract class ModelReferencer {

    public abstract Delete(): void;

    public abstract GetReferences(): Model[];

    public abstract CollectReferences(): void;

    public constructor() {

    }

}