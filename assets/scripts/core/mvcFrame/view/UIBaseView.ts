
import {Model} from "../Model";
import BindComponent from "../base/BindComponent";

const {ccclass, property} = cc._decorator;
/*
*ClassName:UIBaseView
*Description:基础View
*Date:2019/10/25 11:39
*/
@ccclass
export default class UIBaseView extends BindComponent {

    protected static prefabUrl = null;
    protected static className = null;


    /**
     * 得到prefab的路径，相对于resources目录
     */
    public static getUrl(): string {
        return this.prefabUrl;
    }

    /**
     * 类名，用于给UI命名
     */
    public static getName(): string {
        return this.className;
    }

    protected onLoad(): void {
        super.onLoad();
    }

    protected start(): void {
    }

    protected onEnable(): void {

    }

    protected onDisable(): void {
    }

    public Show(): void {

    }

    public UpdateView(data: Object) {

    }

    public UpdateViewByType(model:Model,type:NotifyViewType){

    }


}

export interface UIClass<T extends UIBaseView> {
    new(): T;
    getUrl(): string;
    getName(): string;
}


/**界面更新*/
export enum NotifyViewType {
    show = 1,
    hide = 2,
    update = 3,
    sort = 4
}