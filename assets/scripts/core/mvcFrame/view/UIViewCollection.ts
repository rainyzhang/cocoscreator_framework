import UIBaseView from "./UIBaseView";
import UIView from "./UIView";
import {BaseEventMessage} from "../TMessage";

const {ccclass, property} = cc._decorator;
/*
*ClassName:UIViewCollection
*Description: View集合
*Date:2019/10/25 12:32
*/
@ccclass
export default class UIViewCollection extends UIBaseView {

    public _collectionMap: Map<string, UIView> = new Map<string, UIView>();


    protected onDestroy(): void {
        this._collectionMap.clear();
    }

    public AddView(viewName: string, view: UIView): boolean {
        if (!this._collectionMap.has(viewName)) {
            this._collectionMap.set(viewName, view);
            return true;
        }
        return false;
    }

    public DeleteView(viewName: string) {
        if (!this._collectionMap.has(viewName)) {
            return;
        }
        this._collectionMap.delete(viewName);
    }

    public GetLength(): number {
        return this._collectionMap.size;
    }

    public ShowPanel(uiClass: string, baseEventMessage: BaseEventMessage = null, isShow: boolean = true) {
        if (!isShow) {
            return;
        }
        if (!this._collectionMap.has(uiClass)) {
            return;
        }
        this._collectionMap.get(uiClass).Show(baseEventMessage);
    }

    public ShowOneUIView(uiClass: string, baseEventMessage: BaseEventMessage = null, isShow: boolean = true) {
        if (!isShow) {
            return;
        }
        this._collectionMap.forEach((viewItem: UIView, viewName: string) => {
            if (viewName == uiClass) {
                viewItem.Show(baseEventMessage);
            } else {
                viewItem.Hide();
            }
        })
    }

    public IsUIView(uiClass: string): boolean {
        return this._collectionMap.has(uiClass);
    }

    public ShowOneView(view: UIView) {
        this._collectionMap.forEach((viewItem: UIView, viewName) => {
            if (viewItem == view) {
                viewItem.Show();
            } else {
                viewItem.Hide();
            }
        })
    }

    public GetUIView(uiClass: string): UIView {
        this._collectionMap.forEach((viewItem: UIView, viewName) => {
            if (viewName == uiClass) {
                return viewItem;
            }
        })
        return null;
    }

    public HideAllUIView() {
        if (this._collectionMap.size <= 0) {
            return;
        }
        this._collectionMap.forEach((viewItem: UIView, viewName) => {
            viewItem.Hide();
        })
    }
}