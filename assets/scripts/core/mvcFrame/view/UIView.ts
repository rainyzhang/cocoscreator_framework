import UIBaseView from "./UIBaseView";
import {BaseEventMessage} from "../TMessage";

const {ccclass, property} = cc._decorator;
/*
*ClassName:UIView
*Description:TODO
*Date:2019/10/25 12:07
*/
@ccclass
export default class UIView extends UIBaseView {

    @property(cc.Node)
    public content: cc.Node = null;

    private _resMgr: any = null;
    private _uiMgr: any = null;
    private _url: string = "";
    //是否卸载
    public isNeedUninstall: boolean = false;

    initLoader(url: string, resMgr: any,uiMgr:any) {
        this._url = url;
        this._resMgr = resMgr;
        this._uiMgr = uiMgr;
    }

    public onLoad() {
        super.onLoad();
        if(this.content){
            this.content.active = false;
        }

    }

    public Show(msg:BaseEventMessage = null) {
        super.Show();

        if(this._uiMgr){
            this.node.parent = this._uiMgr.Parent;
        }

        if(this.content){
            this.content.active = true;
        }
    }
    public Hide() {
        if(this._uiMgr){
            let _uiMgr = this._uiMgr;

            if (this.isNeedUninstall) {
                console.log("卸载内存 =", this.node.name);
                this.node.destroy();
                _uiMgr.DeleteView(this.node.name);
            } else {
                console.log("缓存内存 =", this.node.name);
                this.node.stopAllActions();
                if(this.content){
                    this.content.active = false;
                }
                // this.node.removeFromParent(false);
            }
        }else {
            if(this.content){
                this.content.active = false;
            }
        }

    }
}