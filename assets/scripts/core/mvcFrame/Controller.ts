import {Model} from "./Model";
import UIBaseView, {NotifyViewType, UIClass} from "./view/UIBaseView";
import UIView from "./view/UIView";

const {ccclass, property} = cc._decorator;

/*
*ClassName:Controller
*Description:TODO
*Date:2019/10/7 15:14
*/
@ccclass
export abstract class AbstractController extends cc.Component {

    protected onLoad() {
    }

    protected start() {
    }

    protected onEnable() {
    }

    protected onDisable() {
    }

    protected onDestroy() {
    }
}


export abstract class Controller extends AbstractController {

    private _model: Model;

    public get GetModel(): Model {
        return this._model;
    }

    protected onLoad() {
        super.onLoad();
    }

    protected onDestroy() {
        super.onDestroy();
        this.UnBindModel();
    }

    private _uiBaseView: UIBaseView = null;

    protected get view(): UIBaseView {
        return this._uiBaseView;
    }

    protected set view(uiBaseView: UIBaseView) {
        this._uiBaseView = uiBaseView;
    }

    public ViewInstance<T extends UIView>(): UIBaseView {
        if (this._uiBaseView == null) {
            this._uiBaseView = this.getComponent(UIBaseView);
        }
        return <T>this._uiBaseView;
    }


    public BindModel(bindModel: Model) {
        this._model = bindModel;
        this._model.AddChangeListener(this.OnModelChanged);
        this._model.AddDeleteListener(this.OnModelDelete);
    }

    public UnBindModel() {
        if (this._model != null) {
            this._model.RemoveChangeListener(this.OnModelChanged);
            this._model.RemoveDeleteListener(this.OnModelDelete);
            this._model = null;
        }
    }

    protected OnModelChanged(notifyViewType: NotifyViewType) {

    }

    protected OnModelDelete() {
        this.UnBindModel();
        this.node.destroy();
    }


}