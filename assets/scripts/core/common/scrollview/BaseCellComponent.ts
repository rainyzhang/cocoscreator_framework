import {Model} from "../../mvcFrame/Model";
import {CCComponent} from "../../base/CCComponent";
// import CCComponent from "../../mvcFrame/base/CCComponent";

const {ccclass, property} = cc._decorator;
/*
*ClassName:BaseCellComponent
*Description:TODO
*Date:2019/10/26 23:03
*/
@ccclass
export default class BaseCellComponent extends CCComponent {

    public index:number;
    public UpdateViewModel(model: Model, index?: number) {
        this.index = index;
    }
}