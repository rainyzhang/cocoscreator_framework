import Debug, {LogLevel} from "./core/logSystem/Debug";
import TMessage, {BaseEventMessage} from "./core/mvcFrame/TMessage";
import BaseMessage from "./core/mvcFrame/BaseMessage";
import {Model} from "./core/mvcFrame/Model";
import PlayerModel from "./model/PlayerModel";

const {ccclass, property} = cc._decorator;
/*
*ClassName:AppStart
*Description:TODO
*Date:2019/10/7 13:57
*/
@ccclass
export default class AppStart extends cc.Component {

    onLoad() {

        Debug.SetLevel(LogLevel.DEBUG);

        // TMessage.AddListener(SystemMsg,this._systemMsgCallBack,this);
        //
        // TMessage.AddListener<PlayerMsg>(PlayerMsg,this._playerMsgCallBack,this);
        //
        // let ac: TestModel = new TestModel();
        // console.error("testModel =",ac)
        //
        // let ab: PlayerModel = new PlayerModel();
        // ab.id = (1).toString();
        //
        // console.error("player =",ab)
        // console.error("Model.instances len=",Model.instances.length)
        // console.error("Model.instances =",Model.instances)
        // console.error("typeSortedInstances =",Model.typeSortedInstances);
        // // TMessage.RegisterListener(SystemEvent,()=>{
        // //     Debug.error("......111111");
        // // },this,false)
        let ad =  Model.FindModelByT<PlayerModel>((1).toString());
        //
        // let ah = Model.GetAllModelList<PlayerModel>(PlayerModel);
        //
        let play = Model.GetModelById<PlayerModel>(PlayerModel,(1).toString());
        //
         let af =  PlayerModel.GetAllList();
        // console.error("ad player =",af)
    }



    _systemMsgCallBack(bassMsg : SystemMsg){
        if(bassMsg == null){
            return;
        }
        Debug.error("......111111",bassMsg.sysName);
    }

    _playerMsgCallBack(bassMsg : PlayerMsg){
        if(bassMsg == null){
            return;
        }
        Debug.error("......2222",bassMsg.sysName);
    }

    start() {


        // let system :SystemMsg= new SystemMsg();
        // system.sysName = "wasdasd";
        // TMessage.NotifyMessage(SystemMsg,system);
        //
        // let system2 :PlayerMsg= new PlayerMsg();
        // system2.sysName = "FFFFF";
        // TMessage.NotifyMessage(PlayerMsg,system2);
    }

}

export class SystemMsg extends BaseEventMessage{
    protected static className = "SystemMsg";
    public sysName:string;
}

export class PlayerMsg extends BaseEventMessage{
    protected static className = "PlayerMsg";
    public sysName:string;
}

export class TestModel extends Model{
    public name:string;

    public constructor(){
        super("TestModel");

    }
}

